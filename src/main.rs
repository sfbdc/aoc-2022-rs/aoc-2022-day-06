fn main() {
    let input = include_str!("../input.txt");
    println!("Solution A: {}", solve_for_a(input));
    println!("Solution B: {}", solve_for_b(input));
}

fn contains_duplicate(buf: &str) -> bool {
    for ch in buf.chars() {
        if buf.matches(ch).count() > 1 {
            return true;
        }
    }
    false
}

fn find_marker(input: &str, length: usize) -> Option<usize> {
    for idx in 0..input.len() {
        let buf = &input[idx..(idx + length)];
        if !contains_duplicate(buf) {
            return Some(idx + length);
        }
    }

    None
}

fn solve_for_a(input: &str) -> usize {
    let marker_end_index = find_marker(input, 4);
    marker_end_index.unwrap()
}

fn solve_for_b(input: &str) -> usize {
    let marker_end_index = find_marker(input, 14);
    marker_end_index.unwrap()
}

#[test]
fn test_solve_for_a() {
    let input = include_str!("../test.txt");
    let solutions = [7, 5, 6, 10, 11];
    for (indx, line) in input.lines().enumerate() {
        assert_eq!(solutions[indx], solve_for_a(line));
    }
}

#[test]
fn test_solve_for_b() {
    let input = include_str!("../test.txt");
    let solutions = [19, 23, 23, 29, 26];
    for (indx, line) in input.lines().enumerate() {
        assert_eq!(solutions[indx], solve_for_b(line));
    }
}
